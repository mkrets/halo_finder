from setuptools import setup

setup(name='halo_finder',
      version='0.1',
      description='a tool for RAMSES to track halos',
      url='https://bitbucket.org/mkrets/',
      author='mkrets',
      author_email='no@mail.com',
      license='MIT',
      packages=['halo_finder'],
      zip_safe=False)
