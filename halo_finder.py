#!/usr/bin/env python3

from argparse import ArgumentParser

if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument("-p", "--particles", action='store_true',
                        help="find all particles in the final halo")

    parser.add_argument("-l", "--list", action='store_true',
                        help="assign particles to halos in every snapshot")

    parser.add_argument("-t", "--tracker", action='store_true',
                        help="track the halo back in time")

    parser.add_argument("-d", "--decontaminate", action='store_true',
                        help="decontaminate a halo")

    parser.add_argument("-i", "--isolation", action='store_true',
                        help="select halos based on isolation")

    args = parser.parse_args()
    print(args)

    if args.particles:
        from halo_finder import halo_particles
        print("imported particles")
        halo_particles.main()

    if args.list:
        from halo_finder import halo_list
        print("imported list")
        halo_list.main()

    if args.tracker:
        from halo_finder import halo_tracker
        print("imported tracker")
        halo_tracker.main()

    if args.decontaminate:
        from halo_finder import halo_decontamination
        print("imported decontamination")
        halo_decontamination.main()

    if args.isolation:
        from halo_finder import halo_isolation
        print("imported isolation")
        halo_isolation.main()
