#!/usr/bin/env python

import numpy as np
import glob
import os
import pynbody
import matplotlib
try:
    matplotlib.use('agg')
except ValueError:
    pass
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from configparser import ConfigParser
from scipy.optimize import curve_fit
from .halo_finder_functions import get_units, read_halos


def halo_function(path):
    arr = read_halos(path)
    if arr is np.nan:
        return np.nan, np.nan, np.nan, np.nan, np.nan

    ids = arr[:, 0].astype(int)
    pos_x = arr[:, 2]
    pos_y = arr[:, 3]
    pos_z = arr[:, 4]
    mass = arr[:, 6]
    return ids, mass, pos_x, pos_y, pos_z


def nfw(r, r_s, rho_s):
    return rho_s/((r/r_s)*(1.0+(r/r_s))**2)


def log_nfw(r, r_s, rho_s):
    return np.log10(nfw(r, r_s, rho_s))


def make_pynbody_pic(data, center, r200, mass_c, halo_nr, halo_candidate, pp):
    data.original_units()

    pynbody.analysis.halo.transformation.translate(data, -center[:, 0])
    data.physical_units()

    w = 5.0 * r200[0] * c_to_kpc

    sphere = pynbody.filt.Sphere(radius='%.2f kpc' % w)
    halo_sphere = data[sphere]

    prof = pynbody.analysis.profile.Profile(
        halo_sphere.dm, ndim=3, min=1.0, max=(5.*r200[0] * c_to_kpc),
        nbins=75, type="log")

    prof_2_fit = pynbody.analysis.profile.Profile(
        halo_sphere.dm, ndim=3, min=1.0, max=(r200[0] * c_to_kpc),
        nbins=75, type="log")

    mask = prof_2_fit["density"] > 0.0
    popt, pcov = curve_fit(log_nfw, prof_2_fit["rbins"][mask],
                           np.log10(prof_2_fit["density"][mask]))
    concentration = r200[0] * c_to_kpc / popt[0]

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(15, 5))

    im = pynbody.plot.image(halo_sphere.dm, width='%.2f kpc' % (w * 2),
                            log=True, cmap="magma", resolution=500,
                            ret_im=True, units='Msol kpc^-2', vmin=1e4,
                            vmax=3e8, subplot=axes[0], show_cbar=False)

    circle1 = plt.Circle((0, 0), r200[0] * c_to_kpc, color='white',
                         fill=False, linestyle="--", label="$r_{200}$")
    circle2 = plt.Circle((0, 0), w, color='white', fill=False,
                         linestyle="--", label="$5 * r_{200}$")

    axes[0].add_artist(circle1)
    axes[0].add_artist(circle2)

    axes[0].legend(handles=[circle1, circle2])

    axes[0].set_xlabel("x [kpc]")
    axes[0].set_ylabel("y [kpc]")

    fig.colorbar(im, ax=axes[0],
                 label=r"$\Sigma \mathrm{[M_\odot / kpc^{2}]}$")

    axes[1].plot(prof["rbins"], prof["density"], label=r"$\rho(r)$")
    axes[1].axvline(r200[0] * c_to_kpc, ls="-", color="black", lw=1.,
                    label=r"$R_{200}=$"
                    + "{:.1f} kpc \n".format(r200[0] * c_to_kpc)
                    + r"$M_{200}=$"
                    + r"{:.2g} M$_\odot$".format(mass_c[0] * c_to_msun))
    axes[1].axvline(popt[0], ls="--", color="black", lw=1.,
                    label="NFW $r_s={:.1f}$ kpc".format(popt[0]))
    axes[1].plot(prof_2_fit["rbins"], nfw(prof_2_fit["rbins"], *popt),
                 label="NFW $c={:.1f}$".format(concentration))

    axes[1].set_xlim(1., 5.*r200[0] * c_to_kpc)
    axes[1].set_xscale("log")
    axes[1].set_yscale("log")
    axes[1].set_xlabel("r [kpc]")
    axes[1].set_ylabel(r"$\rho$ [M$_\odot/kpc^3$]")
    axes[1].legend(loc=3, framealpha=.7)

    fig.suptitle("halo: " + str(halo_nr) + ", halo ID: " + str(halo_candidate))

    data.original_units()
    pynbody.analysis.halo.transformation.translate(data, +center[:, 0])

    with open(base_path + "part_ids/halo_ids_from_iso.txt", 'ab') as fi:
        np.savetxt(fi, fmt='%i %i %f %f %f %f',
                   X=np.c_[halo_nr,
                           halo_candidate,
                           mass_c[0] * c_to_msun,
                           r200[0] * c_to_kpc,
                           popt[0],
                           concentration])

    pp.savefig()
    plt.close()
    return None


def main():
    global base_path
    base_path = ''

    config = ConfigParser()
    homedir = os.path.expanduser('~')
    config.read(homedir + "/.halo_config.cfg")

    min_mass = config.getfloat('isolation', 'min_mass')
    max_mass = config.getfloat('isolation', 'max_mass')
    search_radius = config.getfloat('isolation', 'search_radius')
    mass_fraction_nb = config.getfloat('isolation', 'mass_fraction_nb')

    global c_to_kpc
    global c_to_msun

    outputs = np.sort(glob.glob(base_path + "output_?????"))[::-1]

    path = outputs[0]
    print(path)

    ids, mass, pos_x, pos_y, pos_z = halo_function(path)
    aexp, unit_m, unit_l, unit_d = get_units(path)

    c_to_kpc = unit_l * 3.24078e-22
    c_to_msun = unit_m / 2e33

    mass_range_mask = np.where(
        (mass * c_to_msun >= min_mass) & (mass * c_to_msun <= max_mass))
    print("mass range: {:1.1e} < m < {:1.1e} [Msun]".format(
        min_mass, max_mass))
    print("found " + str(len(mass_range_mask[0])) + " halos")

    canditate_ids = ids[mass_range_mask]

    data = pynbody.load(path)
    data.physical_units()

    if not os.path.exists(base_path + "part_ids"):
        os.makedirs(base_path + "part_ids")
        print("creating directory:", base_path + "part_ids")

    elif os.path.isfile(base_path + "part_ids/halo_ids_from_iso.txt"):
        os.remove(base_path + "part_ids/halo_ids_from_iso.txt")
        print("deleting previous halo_ids_from_iso.txt")

    pp = PdfPages(base_path + "part_ids/halo_candidates.pdf")

    print("applying isolation criterion")
    print("maximum neighboor mass {:} * M_halo inside {:} R_200".format(
          mass_fraction_nb, search_radius))
    print("ID, candidate")

    halo_nr = 0
    for halo_candidate in canditate_ids:
        halo_mask = halo_candidate == ids
        c_x = pos_x[halo_mask]
        c_y = pos_y[halo_mask]
        c_z = pos_z[halo_mask]

        mass_c = mass[halo_mask]
        r200_estimate = (mass_c * 3. / (200. * 4. * np.pi))**(1.0 / 3.0)

        dist = np.sqrt((pos_x - c_x)**2.
                       + (pos_y - c_y)**2.
                       + (pos_z - c_z)**2.)

        nb_mask = dist < search_radius * r200_estimate

        nb_mass = mass[nb_mask]
        massive_nb = np.sum(nb_mass > mass_c * mass_fraction_nb) - 1.0

        if massive_nb == 0:
            center = np.array([c_x, c_y, c_z])

            make_pynbody_pic(data, center, r200_estimate, mass_c,
                             halo_nr, halo_candidate, pp)

            print(halo_nr, halo_candidate)
            halo_nr = halo_nr + 1

    pp.close()
    return None


if __name__ == "__main__":
    main()
