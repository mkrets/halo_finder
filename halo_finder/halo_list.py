#!/usr/bin/env python3

import numpy as np
import glob
import gc
from functools import partial
from multiprocessing import Pool, cpu_count
from .halo_finder_functions import (read_clumps, read_clump_id_file,
                                    read_ramses_part_id, read_halos,
                                    collect_the_halo)


def halo_loop(halo_file, all_part, all_id_clump, all_clumps_in_halos,
              lookup_table):
    print("read final halo particles", flush=True)
    ids_in_final_halo = np.load(halo_file)

    mask = np.in1d(all_part, ids_in_final_halo)
    clump_list = all_id_clump[mask]

    masker = np.array([clump in all_clumps_in_halos for clump in clump_list])

    org_positions = np.arange(len(clump_list))[masker]
    clump_list = clump_list[masker]

    result = [lookup_table[clump] for clump in clump_list]

    print("saving...", flush=True)
    particle_halo_list = np.zeros_like(ids_in_final_halo)
    for j, resu in enumerate(result):
        particle_halo_list[org_positions[j]] = resu

    np.save(base_path + "part_ids/halo_" +
            halo_file[-7:-4] + "_" + path[-5:], particle_halo_list)
    return None


def the_main_function(path, path_to_file, base_path):
    print("reading clump_id file")
    all_id_clump = read_clump_id_file(path)
    print("reading all ramses particle ids")
    all_part, all_fam = read_ramses_part_id(path)

    only_dm_mask = all_fam == 1
    if np.all(np.isnan(all_id_clump)):
        print("No clump ids found!")
        return None
    all_id_clump = all_id_clump[only_dm_mask]
    all_part = all_part[only_dm_mask]

    print("reading all halos and sort them")
    arr = read_halos(path)
    if np.all(np.isnan(arr)):
        print("No halos found!")
        return None

    ids = arr[:, 0].astype(int)
    ncell = arr[:, 1].astype(int)

    sorted_idx = np.argsort(ncell)[::-1]
    ids = ids[sorted_idx]
    ncell = ncell[sorted_idx]

    print("reading all clumps")
    all_index, all_parent, _ = read_clumps(path)
    if np.all(np.isnan(all_index)):
        print("No clump ids found!")
        return None

    print("collect the halo")
    clump_ids_in_halo = []
    halo_ids = []
    all_clumps_in_halos = set()
    lookup_table = dict()

    for cl_id in ids:
        this_set_clumps = collect_the_halo(all_index, all_parent, cl_id,
                                           return_set=True)
        all_clumps_in_halos.update(this_set_clumps)
        clump_ids_in_halo.extend(list(this_set_clumps))
        halo_ids.extend([cl_id]*len(this_set_clumps))

    for j, cl in enumerate(clump_ids_in_halo):
        lookup_table[cl] = halo_ids[j]

    all_clumps_in_halos.remove(0)
    del clump_ids_in_halo, halo_ids

    if len(path_to_file) > 1:

        halo_loop_partial = partial(
            halo_loop, all_part=all_part, all_id_clump=all_id_clump,
            all_clumps_in_halos=all_clumps_in_halos, lookup_table=lookup_table)

        num_of_cpus = min(len(path_to_file), cpu_count())
        print("reading all halos in parallel on "
              + str(num_of_cpus) + " workers")

        with Pool(num_of_cpus) as pool:
            pool.map(halo_loop_partial, path_to_file)
            pool.close()
            pool.join()

    else:
        print("reading the halo")
        halo_loop(path_to_file[0], all_part, all_id_clump, all_clumps_in_halos,
                  lookup_table)
    return None


def main():
    global base_path
    global path

    base_path = ''

    outputs = np.sort(glob.glob(base_path + "output_?????"))[::-1]

    path_to_file = np.sort(
        glob.glob(base_path + "part_ids/particle_in_halo_???.npy"))

    for path in outputs:
        print(path)
        the_main_function(path, path_to_file, base_path)
        gc.collect()


if __name__ == "__main__":
    main()
