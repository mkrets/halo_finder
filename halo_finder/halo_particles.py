#!/usr/bin/env python

import numpy as np
import glob
import os
import sys

from .halo_finder_functions import (read_clumps, read_halos,
                                    get_dm_count_header, read_clump_id_file,
                                    read_ramses_part_id, collect_the_halo,
                                    get_units)


def halo_function(path):
    arr = read_halos(path)
    if arr is np.nan:
        return np.nan, np.nan, np.nan, np.nan

    aexp, unit_m, unit_l, unit_d = get_units(path)

    ids = arr[:, 0].astype(int)
    ncell = arr[:, 1].astype(int)
    mass = arr[:, 6] * unit_m / 2e33
    return aexp, ids, mass, ncell


def get_halo_of_clump(all_index, all_parent, cl_id):
    parent = all_parent[all_index == cl_id]
    while(cl_id != parent):
        cl_id = parent
        parent = all_parent[all_index == cl_id]

    return parent


def main():
    base_path = ''

    outputs = np.sort(glob.glob(base_path + "output_?????"))
    path = outputs[-1]
    print(path)

    all_index, all_parent, clumps_cells = read_clumps(path)
    aexp, halo_ids, halo_mass, halo_cells = halo_function(path)

    if os.path.isfile(base_path + "part_ids/halo_ids_from_iso.txt"):
        try:
            halo_ids_from_iso = np.genfromtxt(
                base_path + "part_ids/halo_ids_from_iso.txt", dtype=np.int,
                usecols=(1))
        except ValueError:
            halo_ids_from_iso = np.genfromtxt(
                base_path + "part_ids/halo_ids_from_iso.txt", dtype=np.int)

        halo_ids_from_iso = np.atleast_1d(halo_ids_from_iso)

        sys.stdout.write(
            "Halo_id file found! Using "
            "{:3.0f} halos.\n".format(halo_ids_from_iso.size))

    else:
        sys.stdout.write("No Halo_id file found! using most cells \n")
        halo_by_cells = halo_ids[np.argsort(halo_cells)[::-1]]
        halo_by_cells = halo_by_cells[:1]

        halo_ids_from_iso = all_parent[np.argsort(clumps_cells)[::-1]]
        halo_ids_from_iso = get_halo_of_clump(all_index, all_parent,
                                              halo_ids_from_iso[:1])

        if not np.array_equal(halo_by_cells, halo_ids_from_iso):
            print("------", flush=True)
            print("Clump with most cells is not in halo with most cells.",
                  flush=True)
            print("------", flush=True)

    tot_part_header = get_dm_count_header(path)
    all_id_clump = read_clump_id_file(path)
    all_part, all_fam = read_ramses_part_id(path)

    only_dm_mask = all_fam == 1
    all_id_clump = all_id_clump[only_dm_mask]
    all_part = all_part[only_dm_mask]

    assert len(all_id_clump) == len(all_part)
    assert len(all_id_clump) == tot_part_header

    sys.stdout.write("# aexp = {:1.4}\n".format(aexp))
    sys.stdout.write("# halo_nr    id    mass   cells\n")

    for halo_nr, cl_id in enumerate(halo_ids_from_iso):
        mask_id = halo_ids == cl_id
        sys.stdout.write('{:6.0f}'.format(halo_nr))
        sys.stdout.write("    ")
        sys.stdout.write('{:6.0f}'.format(halo_ids[mask_id][0]))
        sys.stdout.write("    ")
        sys.stdout.write('{:1.4e}'.format(halo_mass[mask_id][0]))
        sys.stdout.write("    ")
        sys.stdout.write('{:6.0f}'.format(halo_cells[mask_id][0]))
        sys.stdout.write('\n')

        clump_ids_in_halo = collect_the_halo(all_index, all_parent, cl_id)
        mask = np.in1d(all_id_clump, clump_ids_in_halo)
        part_id_in_halo = all_part[mask]

        if not os.path.exists(base_path + "part_ids"):
            os.makedirs(base_path + "part_ids")
            print("creating:", base_path + "part_ids")

        np.save(base_path + "part_ids/particle_in_halo_" +
                str(halo_nr).zfill(3), part_id_in_halo)


if __name__ == "__main__":
    main()
