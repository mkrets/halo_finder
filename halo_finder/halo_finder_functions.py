#!/usr/bin/env python3
import numpy as np
import glob
import os
from multiprocessing import Pool, cpu_count
from scipy.io import FortranFile
from pandas import read_csv

nthreads = os.getenv('OMP_NUM_THREADS')
if nthreads is None:
    nthreads = 8

print("python: using {} threads".format(nthreads))


def parallel_map(read_function, allFiles):
    core_count = min(int(nthreads), cpu_count())
    with Pool(core_count) as pool:
        df_list = pool.map(read_function, allFiles)
        pool.close()
        pool.join()
    return np.array(df_list)


def read_halos(path):
    allFiles = glob.glob(path + "/halo_*")

    all_halos = np.zeros(7)
    for df_ in parallel_map(reader, allFiles):
        if np.any(df_ is None):
            continue
        else:
            all_halos = np.vstack((all_halos, df_))
    if all_halos.sum() == 0:
        return np.full(7, np.nan)
    else:
        return all_halos


def read_clumps(path, ret_pos=False):
    allFiles = glob.glob(path + "/clump_*")
    if len(allFiles) == 0:
        return np.nan

    all_clumps = np.zeros(12)
    for df_ in parallel_map(reader, allFiles):
        if np.any(df_ is None):
            continue
        else:
            all_clumps = np.vstack((all_clumps, df_))

    if all_clumps.sum() == 0:
        return np.full(3, np.nan)

    elif ret_pos:
        return (all_clumps[:, 0].astype(np.int),
                all_clumps[:, 2].astype(np.int),
                all_clumps[:, 3].astype(np.int),
                all_clumps[:, 4:7])
    else:
        return (all_clumps[:, 0].astype(np.int),
                all_clumps[:, 2].astype(np.int),
                all_clumps[:, 3].astype(np.int))


def read_ramses_part_id(path):
    allFiles = glob.glob(path + "/part_?????.out?????")
    allFiles = np.sort(allFiles)

    if len(allFiles) == 0:
        return np.nan, np.nan

    df_ = parallel_map(part_id_reader, allFiles)

    return np.concatenate(df_[:, 0]), np.concatenate(df_[:, 1])


def read_clump_id_file(path):
    allFiles = glob.glob(path + "/id_clump.out?????")
    allFiles = np.sort(allFiles)

    if len(allFiles) == 0:
        print("no clump files")
        return np.atleast_1d(np.nan)

    df_ = parallel_map(clump_id_reader, allFiles)

    return np.concatenate(df_)


def reader(filename):
    df = read_csv(filename, delim_whitespace=True)
    if len(df) == 0:
        return None
    return df.values


def part_id_reader(file_):
    infile = FortranFile(file_, 'r')

    infile.read_ints()
    infile.read_ints()
    npart = infile.read_ints('i')
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()

    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    identity = infile.read_ints()

    infile.read_ints('i4')
    family = infile.read_ints('i1')
    infile.close()
    assert int(npart) == len(identity)
    assert int(npart) == len(family)
    return identity, family


def clump_id_reader(file_):
    infile = FortranFile(file_, 'r')

    data = infile.read_ints('i')

    infile.close()
    return data


def get_dm_count_header(path):
    header = glob.glob(path + "/header*")

    with open(header[0], "r") as head:
        dm_head = head.readlines()

    tot_part_header = int(dm_head[7][13:])
    return tot_part_header


def collect_the_halo(all_index, all_parent, cl_id, return_set=False):
    all_clump_ids = set()
    children = all_index[all_parent == cl_id]
    all_clump_ids.update(children)

    for i in range(7):
        children = all_index[np.in1d(all_parent, children)]
        all_clump_ids.update(children)

    if return_set:
        return all_clump_ids
    else:
        return np.array(list(all_clump_ids))


def get_units(path):
    filename = glob.glob(path + "/info_?????.txt")[0]

    f = open(filename, "r")
    lines = f.readlines()
    f.close()

    for line in lines:
        if line[0:13] == "aexp        =":
            aexp = float(line[14:-1])
        if line[0:13] == "unit_l      =":
            unit_l = float(line[14:-1])
        if line[0:13] == "unit_d      =":
            unit_d = float(line[14:-1])

    unit_m = unit_l**3 * unit_d

    return aexp, unit_m, unit_l, unit_d
