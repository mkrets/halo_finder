#!/usr/bin/env python

import numpy as np
import glob
import gc
import os
import pynbody
from scipy.spatial import ConvexHull
from configparser import ConfigParser
from .halo_finder_functions import read_halos, get_units


def music_log_reader(path_to_music_log):
    with open(path_to_music_log, "r") as f:
        for line in f.readlines():
            if line.find("setup/shift_x") > 0:
                shift_x = line.split("= ")[1]
            if line.find("setup/shift_y") > 0:
                shift_y = line.split("= ")[1]
            if line.find("setup/shift_z") > 0:
                shift_z = line.split("= ")[1]
            if line.find("setup/levelmin =") > 0:
                levelmin = line.split("= ")[1]
                levelmin = int(levelmin)

    ps_s = np.array([shift_x, shift_y, shift_z]).astype(np.float)
    return ps_s, levelmin


def main_function(halo_nr):
    config = ConfigParser()
    homedir = os.path.expanduser('~')
    config.read(homedir + "/.halo_config.cfg")

    path_to_music_log_rel = config.get('decontamination',
                                       'path_to_music_log_rel')
    output_particles = config.getboolean('decontamination', 'output_particles')
    ps_str = config.get('decontamination', 'point_shift')
    levelmin = config.getint('decontamination', 'levelmin')

    ps_s = np.array(ps_str.split(",")).astype(int)

    path_to_music_log = glob.glob(base_path + path_to_music_log_rel)
    if len(path_to_music_log) != 1:
        print("there is a problem with the music path")
        print("using point_shift and levelmin from config")
    else:
        ps_s, levelmin = music_log_reader(path_to_music_log[0])

    outputs = np.sort(glob.glob(base_path + "output_?????"))[::-1]
    path = outputs[0]
    first = outputs[-1]
    print(path)
    last_output_nr = path.split("/")[-1][-5:]

    my_selected_halo_id = np.float(
        np.unique(np.load(base_path + "part_ids/halo_"
                          + str(halo_nr).zfill(3) + "_" + last_output_nr
                          + ".npy")))

    data = pynbody.load(path)
    data.physical_units()

    all_halos = read_halos(path)
    idx = all_halos[:, 0].astype(int)
    pos = all_halos[:, 2:5]
    mass = all_halos[:, 6]
    r200_estimate = (mass * 3. / (200. * 4. * np.pi))**(1.0 / 3.0)

    aexp, unit_m, unit_l, unit_d = get_units(path)
    c_to_kpc = unit_l * 3.24078e-22

    center = pos[idx == my_selected_halo_id] * c_to_kpc
    r200_estimate = r200_estimate[idx == my_selected_halo_id][0] * c_to_kpc

    pynbody.analysis.halo.transformation.inverse_translate(data, center)

    inrvirsaved = np.load(base_path + "part_ids/particle_in_halo_" +
                          str(halo_nr).zfill(3) + ".npy")

    for trace_back in np.linspace(1.0, 1.7, 12):
        w = r200_estimate * trace_back
        sphere = pynbody.filt.Sphere(radius='%.3f kpc' % w)
        halo_sphere = data.dm[sphere]

        found = np.sum(np.in1d(halo_sphere["iord"], inrvirsaved))
        if float(found) / len(inrvirsaved) > 0.999:
            print('| Traceback radius    = {0} R_vir'.format(trace_back))
            break

    print('| Mass in traceback   = {0:.3g}'.format(
          halo_sphere.dm["mass"].sum()))

    data_f = pynbody.load(first)
    data_f.original_units()

    at_init = np.in1d(data_f.dm["iord"], halo_sphere["iord"])

    coarse_in_rtb_init = data_f.dm['mass'][at_init] > 1.1 * \
        np.min(data_f.dm['mass'])
    previously_zommed = data_f.dm['mass'][at_init] < 1.1 * \
        np.min(data_f.dm['mass'])

    print('| Coarse particles    = {0}'.format(
          coarse_in_rtb_init[coarse_in_rtb_init].size))

    hull = ConvexHull(data_f.dm['pos'][at_init])
    hull_zoom = ConvexHull(data_f.dm['pos'][at_init][previously_zommed])
    print('| Volume increase     = {0:.2f}%'.format(
          100 * (hull.volume / hull_zoom.volume) - 100.))

    shift = ps_s/2.0**levelmin

    print('| Point shift         = {0}'.format(ps_s))
    print('| levelmin            = {0}'.format(levelmin))

    unit_l_first = get_units(first)[2]
    c_to_kpc_first = unit_l_first * 3.24078e-22
    print('| Zoom volume (cu)    = {0:.2g}'.format(hull.volume))
    print('| Zoom volume (kpc^3) = {0:.1f}'.format(hull.volume *
                                                   c_to_kpc_first**3))

    np.savetxt(base_path + "part_ids/trace_back_dec_" +
               str(halo_nr).zfill(3),
               data_f.dm['pos'][at_init][hull.vertices]-shift)

    """
    np.savetxt(base_path + "part_ids/trace_back_dec_cen_" +
               str(halo_nr).zfill(3), data_f.dm['pos'][at_init][hull.vertices])
    """

    if output_particles:
        np.savetxt(base_path + "part_ids/trace_back_dec_" +
                   str(halo_nr).zfill(3) + "_part",
                   data_f.dm['pos'][at_init]-shift)

    gc.collect()


def main():
    global base_path
    base_path = ''

    halo_nr = 0
    if os.path.isfile(base_path + "part_ids/halos_to_be_traced.txt"):
        all_halo_nr = np.genfromtxt(base_path
                                    + "part_ids/halos_to_be_traced.txt",
                                    dtype=int)
        for halo_nr in all_halo_nr:
            print("looking at Halo number:", halo_nr)
            main_function(halo_nr)
    else:
        print("Only found one main Halo.")
        main_function(halo_nr)


if __name__ == "__main__":
    main()
