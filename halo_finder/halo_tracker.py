#!/usr/bin/env python

import numpy as np
import glob
import gc
from scipy.io import FortranFile
import warnings

from os.path import isfile

from .halo_finder_functions import (read_clumps, collect_the_halo, get_units,
                                    read_halos)

warnings.filterwarnings("ignore")


def halo_function(path, ret_corrds=False):
    arr = read_halos(path)
    if arr is np.nan:
        return np.nan, np.nan, np.nan

    aexp, unit_m, unit_l, unit_d = get_units(path)

    ids = arr[:, 0].astype(int)
    mass = arr[:, 6] * unit_m / 2e33
    if ret_corrds:
        return aexp, ids, mass, arr[:, 2:5]
    else:
        return aexp, ids, mass


def load_halo_ids_part(path, halo_nr):
    try:
        halo_ids_out = np.load(base_path + "part_ids/halo_" +
                               str(halo_nr).zfill(3) + "_" + path[-5:] +
                               ".npy")
    except OSError:
        return np.nan, np.nan
    ha_id, ha_counts = np.unique(halo_ids_out, return_counts=True)
    return ha_id, ha_counts


def part_mass_reader(file_):
    infile = FortranFile(file_)

    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()
    infile.read_ints()

    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    infile.read_reals('d')
    mass = infile.read_reals('d')
    infile.close()
    return mass


def particle_mass(path):
    for part_file in glob.glob(path + '/part_?????.out?????'):
        mass = part_mass_reader(part_file)
        if len(mass) > 100:
            break

    aexp, unit_m, unit_l, unit_d = get_units(path)
    dm_part_mass, count = np.unique(mass, return_counts=True)
    return dm_part_mass[np.argmax(count)] * unit_m / 2e33


def centering_on_clumps(path, cl_id):
    all_index, all_parent, cl_cells, cl_pos = read_clumps(path, ret_pos=True)
    clump_ids_in_halo = collect_the_halo(all_index, all_parent, cl_id)
    filtered_clumps = np.in1d(all_index, clump_ids_in_halo)

    cl_cells = cl_cells[filtered_clumps]
    cl_pos = cl_pos[filtered_clumps]

    track = cl_pos[np.argmax(cl_cells)]
    aexp, unit_m, unit_l, unit_d = get_units(path)
    center = track * unit_l * 3.24078e-22
    return track, center


def the_main_loop(outputs, halo_nr):
    max_x = len(outputs)
    max_y = 5

    all_masses_matrix = np.zeros((max_x, max_y))
    all_fracti_matrix = np.zeros((max_x, max_y))

    aexp_list = np.zeros(max_x)

    dm_part_mass = particle_mass(outputs[0])
    halo_position = np.zeros((max_x, 3))
    halo_center = np.zeros((max_x, 3))

    for i, path in enumerate(outputs):
        ha_id, ha_counts = load_halo_ids_part(path, halo_nr)
        if np.any(ha_id is np.nan):
            break
        aexp, ids, mass, pos = halo_function(path, ret_corrds=True)
        aexp, unit_m, unit_l, unit_d = get_units(path)

        aexp_list[i] = aexp

        ha_mass = np.array([])
        for this_halo_id in ha_id:
            mask = this_halo_id == ids
            ha_mass = np.append(ha_mass, mass[mask])

        mask = (ha_counts * dm_part_mass) / ha_mass < 0.6
        ha_mass[mask] = 0.0

        sorted_idx = np.argsort(ha_mass)[::-1]
        ha_counts = ha_counts[sorted_idx]
        ha_mass = ha_mass[sorted_idx]
        ha_id = ha_id[sorted_idx]

        mask = ha_mass > 0.0

        ha_mass = ha_mass[mask]
        ha_counts_non_zero = ha_counts[mask]

        ha_counts_frac = ha_counts_non_zero / float(np.sum(ha_counts))
        all_masses_matrix[i][:len(ha_mass[:max_y])] = ha_mass[:max_y]
        all_fracti_matrix[i][:len(ha_counts_frac[:max_y])
                             ] = ha_counts_frac[:max_y]
        if len(ha_id[mask]) > 0:
            track, center = centering_on_clumps(path, ha_id[mask][0])
            halo_position[i] = track
            halo_center[i] = center
    return (aexp_list, all_masses_matrix, all_fracti_matrix, halo_position,
            halo_center)


def main():
    global base_path
    base_path = ''

    outputs = np.sort(glob.glob(base_path + "output_?????"))[::-1]
    num_of_halos = len(glob.glob(base_path +
                                 "part_ids/particle_in_halo_???.npy"))

    for halo_nr in range(num_of_halos):
        if isfile(base_path + "part_ids/halo_tracker_"
                  + str(halo_nr).zfill(3) + ".npz"):
            print(halo_nr, "file exsits")

        else:
            print(halo_nr, "file does not exsits, running ...")
            aexp_list, all_masses_matrix, all_fracti_matrix, track, center =\
                the_main_loop(outputs, halo_nr)
            np.savez(base_path + "part_ids/halo_tracker_"
                     + str(halo_nr).zfill(3) + ".npz",
                     aexp_list, all_masses_matrix,
                     all_fracti_matrix, track, center)
            gc.collect()

    if num_of_halos == 1:
        print(halo_nr, "fitting trajectory: ")
        halo_nr = 0
        load_from_file = np.load(
            base_path + "part_ids/halo_tracker_"
            + str(halo_nr).zfill(3) + ".npz")

        aexp_list = load_from_file["arr_0"]
        all_masses_matrix = load_from_file["arr_1"]
        all_fracti_matrix = load_from_file["arr_2"]
        track = load_from_file["arr_3"]
        center = load_from_file["arr_4"]

        mask = np.logical_and(track[:, 0] > 0.0, aexp_list > 0.1)

        poly_x = np.polyfit(aexp_list[mask], track[:, 0][mask], 3)
        poly_y = np.polyfit(aexp_list[mask], track[:, 1][mask], 3)
        poly_z = np.polyfit(aexp_list[mask], track[:, 2][mask], 3)

        print('xcentre_frame=' + ','.join(map(str, poly_x[::-1])))
        print('ycentre_frame=' + ','.join(map(str, poly_y[::-1])))
        print('zcentre_frame=' + ','.join(map(str, poly_z[::-1])))

        with open(base_path + "part_ids/track_fitting.txt", "w") as fit_file:
            fit_file.write('xcentre_frame=' +
                           ','.join(map(str, poly_x[::-1])) + "\n")
            fit_file.write('ycentre_frame=' +
                           ','.join(map(str, poly_y[::-1])) + "\n")
            fit_file.write('zcentre_frame=' +
                           ','.join(map(str, poly_z[::-1])) + "\n")

        a_ = aexp_list.reshape(len(aexp_list), 1)
        center_to_file = np.hstack((a_, center))
        track_to_file = np.hstack((a_, track))

        np.savetxt(base_path + "track.txt", track_to_file[::-1],
                   delimiter='    ')
        np.savetxt(base_path + "centers.txt", center_to_file[::-1],
                   delimiter='    ')


if __name__ == "__main__":
    main()
