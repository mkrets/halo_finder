# halo finder

## setup

to install go into `halo_finder` and run
```
pip install .
# or (not recommended):
# python setup.py install
```
It should work with python 2.7 and python 3.

copy/move or link the file `halo_finder.py` to your bin folder. For example:
```
ln -s halo_finder.py $HOME/bin/halo_finder.py
```

make sure the file can be executed:
```
chmod 744 halo_finder.py
```

## runing

halo_finder must be executed in the folder where the outputs are `output_00001, output_00002, ...`
it will create a folder `part_ids`.

## options

a help messeage is provided

```
halo_finder.py -h
```
### halo_isolation:
```
halo_finder.py -i
```
`halo_isolation` intended to be used on a unigird (dark matter only) simulation to select halos based on mass and on isolation
creates a list of halo_ids named `halo_ids_from_iso.txt` and a pdf `halo_candidates.pdf`.
Selection paramters can be set in the config.

### halo_particles:
```
halo_finder.py -p
```
`halo_particles` selects all particles that end up in the final halo. If a file `halo_ids_from_iso.txt` exsists in `part_ids`, all halo ids in there will be selected. Else, the halo with the most cells will be used (e.g for a zoom-in). Writes `particle_in_halo_???.npy` in `part_ids` for each halo. If there is only one halo then there is only `...halo_000...`.

### halo_list:
```
halo_finder.py -l
```
`halo_list` traces back the particles of a halo.
So halo_particles has to be run before. Writes for each halo `halo_???_yyyyy.npy` where `y` is the output number.

### halo_tracker:
```
halo_finder.py -t
```
`halo_tracker` creates a history of the halo. If there is only 1 halo (e.g. zoom-in) then coordinates for ramses movie-tracking are fitted. Additionally `track.txt` and `centers.txt` are created (in the parent directory). They contain the coordinates of the halo in code-units and in physical (kpc) for easy centering in the post-processing. See below how to use the output. Uses outputs from `halo_finder`. Writes `halo_tracker0.npz`, see below how to read.


### halo_decontamination:
```
halo_finder.py -d
```
for zoom-in simulation:
`halo_decontamination` checks for contamination and creates new files for cleaner initial conditions called `trace_back_dec_000`.

## config file
For isolation and decontamination you may want to adjust the config file.
First link or copy it to your home. e.g:
```
ln -s $HOME/.halo_config.cfg halo_config.cfg
```
### isolation
| parameter | default | explanation |
|-----------|---------|-------------|
|`min_mass`| 1e12 | minimum mass for halo selection |
|`max_mass`| 5e12 | maximum mass for halo selection |
|`search_radius`| 5.0 | search radius for massive neighbors in virial radii |
|`mass_fraction_nb`| 0.05 | fraction of mass of neighbors to be excluded |

### decontamination
You can set a path to the folder that include the log-file for initial conditions generated with music.

| parameter | default | explanation |
|-----------|---------|-------------|
|`path_to_music_log_rel`| ../ic/*_log.txt | path to the log-file produced by MUSIC for the used initial conditions |
|`output_particles`| False| output the list of particles after decontamination. Can be used to fit an ellipse in MUSIC
|`point_shift`|  1,2,3 | point shift of the run that is decontaminated |
|`levelmin`| 7 | levelmin of the run that is decontaminated |

---
## Examples

Example a uni-grid DMO simulation.
Run:
```
halo_finder.py -i
```
This will run:
1. `halo_isolation`  
   - check the pdf and the shell-output  
   - maybe adjust the config file

```
halo_finder.py -p -l -t
```
This will run:

2. `halo_particles`
3. `halo_list`
4. `halo_tracker`
---

Example for a zoom simulation (hydro or dm).
Run:

```
halo_finder.py -p -l -t -d
```
This will run:

1. `halo_particles`
2. `halo_list`
3. `halo_tracker`
4. `halo_decontamination`


____
### halo_tracker

you can use something like the code below to load the data.
The halo_tracker*.npz contains a list of 


1. scalefactor
2. a matrix of masses: for each snapshot the 5 most massive halos that contain particles that end up in the final halo (sorted by mass)
3. a matrix of number_fractions: the fraction of particles that end up in the final halo inside the 5 most massive halos in a snapshot
4. coordinates in code units of the most massive halo
5. coordinates in physical units (kpc) of the most massive halo


```python
import os.path

halo_nr = 0

if os.path.isfile("part_ids/halo_tracker" + str(halo_nr) + ".npz"):
    print(halo_nr, "file exsits, loading ...")
    load_from_file = np.load(base_path + "part_ids/halo_tracker" + str(halo_nr) + ".npz")
    
    aexp_list = load_from_file["arr_0"]
    all_masses_matrix = load_from_file["arr_1"]
    all_fracti_matrix = load_from_file["arr_2"]
    track = load_from_file["arr_3"]
    center = load_from_file["arr_4"]
```

## slurm

Here is an example, how to run with slurm:

```bash
#!/bin/bash -l
#SBATCH --job-name=halo
#SBATCH --time=00:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=normal
#SBATCH --mem=12GB

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
echo "using $OMP_NUM_THREADS threads"

export PATH=$HOME/bin:$PATH

srun halo_finder.py -p
```

## jupyter notebook

To use parallelism use first:
```python
%env OMP_NUM_THREADS=16
```
alternatively use:
```python
import os
os.environ["OMP_NUM_THREADS"]="16"
```
then you can import either the whole package:

```python
import halo_finder as hf
```
or specific modules:
```python
from halo_finder import halo_list as hl

path = "some/path/hydro_52/output/output_00086/"
all_part, all_fam = hl.read_ramses_part_id(path)
```
